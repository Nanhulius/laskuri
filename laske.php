<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h3>Nettopalkkalaskuri</h3>
    <?php
    $brutto = filter_input(INPUT_POST, 'bruttopalkka', FILTER_SANITIZE_NUMBER_INT);
    $ennakko = filter_input(INPUT_POST, 'ennakkopidatys', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $elakem = filter_input(INPUT_POST, 'tyoelake', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $vakuus = filter_input(INPUT_POST, 'tyottomyysvak', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    
    $ennakkovah = ($brutto / 100) * $ennakko;
    $elakemvah = ($brutto / 100) * $elakem;
    $vakuusvah = ($brutto / 100) * $vakuus;
    $netto = $brutto - $ennakkovah - $elakemvah - $vakuusvah;

    printf("<p>Bruttopalkkasi on %.2f €", $brutto);
    printf("<p>Ennakkopidätyksesi on %.2f prosenttia, joka on euroissa %.2f €</p>", $ennakko, $ennakkovah);
    printf("<p>Työeläkemaksusi on %.2f prosenttia, joka on euroissa %.2f €</p>", $elakem, $elakemvah);
    printf("<p>Työttömyysvakuutusmaksusi on %.2f prosenttia, joka euroissa on %.2f €</p>", $vakuus, $vakuusvah);
    printf("<p>Nettopalkkasi on siis %.2f €</p>", $netto);
    ?>
    <a href="index.php">Takaisin alkuun</a>
</body>

</html>